# React

## 组件实例三大属性

### state

### props

### refs

## react中的事件处理

## 组件生命周期

### 组件的生命周期(旧)

#### 初始化阶段

- 由ReactDOM.render()触发 初次渲染

  - constructor()
  - componentWillMount()
  - render()
  - componentDidMount()
    - 常用，一般在这个钩子中做一些初始化的事，列如：开启定时器，发送网络请求，订阅消息

#### 更新阶段

- 由组件内部this.setState()或父组件render触发

  - shouldComponentUpdate()
  - componentWillUpdate()
  - render()
  - componentDidUpdate()

#### 卸载组件

- 由ReactDOM.unmountComponentAtNode()触发
  - componentWillUnmount()
    - 常用，一般在这个钩子中做一些收尾的事，列如：关闭定时器，取消订阅消息

### 组件的生命周期(新)

#### 初始化阶段

- 由ReactDOM.render()触发 初次渲染

  - constructor()
  - getDerivedStateFromProps()
  - render()
  - componentDidMount()
    - 常用，一般在这个钩子中做一些初始化的事，列如：开启定时器，发送网络请求，订阅消息

#### 更新阶段

- 由组件内部this.setState()或父组件render触发
  - getDerivedStateFromProps
  - shouldComponentUpdate()
  - render()
  - getSnapshotBeforeUpdate
  - componentDidUpdate()

#### 卸载组件

- 由ReactDOM.unmountComponentAtNode()触发
  - componentWillUnmount()
    - 常用，一般在这个钩子中做一些收尾的事，列如：关闭定时器，取消订阅消息

### 总结

#### 重要的勾子

- render：初始化渲染或更新渲染调用
- componentDidMount：开启监听, 发送ajax请求
- componentWillUnmount：做一些收尾工作, 如: 清理定时器
#### 即将废弃的勾子
- componentWillMount
- componentWillReceiveProps
- componentWillUpdate
现在使用会出现警告，下一个大版本需要加上UNSAFE_前缀才能使用，以后可能会被彻底废弃，不建议使用。