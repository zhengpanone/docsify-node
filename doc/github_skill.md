# github使用技巧

## 浏览code

- 在github仓储url上加上1s通过vscode web浏览代码
  - https://github1s.com/trekhleb/javascript-algorithms/blob/master/README.zh-CN.md

## 搜索

### 按照name搜索

```
in:name Vue stars:>5000 forks:>3000
```

### 按照README来搜索

```
in:readme React stars:>3000 forks:>3000
```

### 按照 descriptin 搜索

```
in:description 微服务 language:python pushed:>2021-07-01
```

### 总结

```
in:name xxx // 按照项目名搜索
in:readme xxx // 按照README搜索
in:description xxx // 按照description搜索


stars:>xxx // stars数大于xxx
forks:>xxx // forks数大于xxx
language:xxx // 编程语言是xxx
pushed:>YYYY-MM-DD // 最后更新时间大于YYYY-MM-DD
```

