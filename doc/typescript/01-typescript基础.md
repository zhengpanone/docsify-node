# TypeScript

## 常用语法

### 基础类型



|  类型   |                    例子                     |                          描述                          |
| :-----: | :-----------------------------------------: | :----------------------------------------------------: |
| number  |                  1,-33,2.5                  |                        任意数字                        |
| string  |           "hello"，'hello'，hello           |                       任意字符串                       |
| boolean |                 true、false                 |                         布尔值                         |
| 字面量  | 其本身(let a:"male"\|"female" 称为联合类型) |              限制变量的值就是该字面量的值              |
|   any   |                      *                      | 任意类型,(变量类型为any相当于对该变量关闭了TS类型检测) |
| unknown |                      *                      |                     类型安全的any                      |
|  void   |              空值（undefined）              |                 没有值（或undefined）                  |
|  never  |                   没有值                    |                      不能是任何值                      |
| object  |                {name:"张三"}                |                       任意js对象                       |
|  array  |                   [1,2,3]                   |                       任意js数组                       |
|  tuple  |                    [4,5]                    |             元素，TS新增类型，固定长度数组             |
|  enum   |                 enum{A, B}                  |                   枚举，TS中新增类型                   |

#### 数据类型

```typescript
let dec: number = 20 // 十进制
let hex: number = 0x14 //十六进制
let binary: number = 0b10100 //二进制
let octal: number = 0o24 //八进制
let big: bigint = 100n
```

#### 字符串类型

```typescript
let name: string = '张三'
let age: number = 29
let sentence = `Hello, my name is ${name}.
I'll be ${age+1} year old next month.
`
```

- 数组

```typescript
let list1: number[] = [1,2,3]
let list2: Array<number> = [1,2,3]
```

- 元组

```typescript
let x: [string,number]
x = ['hello', 10]
```

- 枚举

```typescript
  enum Color {
      Red=2,
      Green,
      Blue
  }
let colorName: string = Color[2]
console.log(colorName);
```

- never
  - 表示永远不会返回结果

```typescript
function error(message: string): never {
    throw new Error("出错了")
}

function fail(){
    return error('something failed')
}

function inifiniteLoop(): never {
    while (true){
        
    }
}
```

- object

```typescript
declare function create(o: object|null):void;
create({prop:0})
create(null)

// {} 用来指定对象中可以包含哪些属性
let b: {name: string}
b = {name: '张三'}
//加上？表示类型可选
let c: {name: string, age?:number}
// [propName: string]:any propName 可以为任意名称,
let d:{name:string,[propName: string]:any}
d = {name: '李四', age: 18, gender:'男'}
/**
* 设置函数结构的类型声明
语法: (形参:类型,形参:类型...) => 返回值
/
// 表示变量f是一个函数类型有两个参数且为number类型,返回一个number类型
let f: (a: number, b: number) => number;
f = function (n1, n2) {
    return n1 + n2;
}
```



### 变量声明

#### let

```javascript
function f(){
    var a = 10
    return function g(){
        var b = a + 1
        return b
    }
}
var g = f()
g()

for(var i = 0; i<10; i++){
    setTimeout(function(){
        console.log(i)
    },100*i)
}

for(var i = 0; i<10; i++){
    (function(j){
        setTimeout(function(){
        console.log(j)
    },100*j)
    })(i)
    
}
```



#### const

#### 解构

### Interface 接口



### 类 Class

OOP三大特征：封装、继承、多态

```typescript
class Animal {
  readonly name: string; //只读属性
  constructor(name: string) {
    this.name = name;
  }
  private run() { // 只能在本类中访问
    return `${this.name} is running`;
  }
  protected eat() {// 可以在本类和子类中访问
    return `${this.name} is eat`;
  }
}
class Dog extends Animal {
  bark() {
    return `${this.name} is barking`;
  }
}

class Cat extends Animal {
  static categories = ["mammal"];
  constructor(name) {
    super(name);
    console.log(this.name);
  }
  eat() {
    return `Meow, ${super.eat()}`;
  }
}
```



### 函数

### 泛型

### 类型推断

```typescript
function getLength(input: string | number): number { //union type
  const str = input as string; // 类型断言
  if (str.length) {
    return str.length;
  } else {
    const number = input as number;
    return number.toString().length;
  }
}
```

### type gurad

```typescript
function getLength2(input: string | number): number {
  if (typeof input === "string") {
    return input.length;
  } else {
    return input.toString().length;
  }
}
```



### 高级类型

### 声明文件

- 自定义声明文件

  - jQuery.d.ts

    ```typescript
    declare var jQuery: (selector:string) => any;
    ```

    

- 下载第三方声明文件

```shell
npm -i --save @types/jquery
```

可以通过github查找DefinitelyTyped 查找第三方声明文件

## 前端常用工具

- Jest
- Commitizen
- RollupJS
- TSLint
- Prettier
- Semantic release
- 