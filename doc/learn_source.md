# 前端学习资源



## 常用资源

[anime.js官网_免费、灵活的轻型JavaScript动画库 | animejs](https://www.animejs.cn/)

[Animate.css | A cross-browser library of CSS animations.](https://animate.style/)

[Hover.css - A collection of CSS3 powered hover effects (ianlunn.github.io)](https://ianlunn.github.io/Hover/)

[Tailwind CSS 中文文档 - 无需离开您的HTML，即可快速建立现代网站。](https://www.tailwindcss.cn/)





## JavaScript

- **[javascript-algorithms （JavaScript 算法与数据结构）](https://github.com/trekhleb/javascript-algorithms.git)**
  - github地址：https://github.com/trekhleb/javascript-algorithms.git
  - YouTub视频地址：https://www.youtube.com/playlist?list=PLLXdhg_r2hKA7DPDsunoDZ-Z769jWn4R8

- [You-Dont-Know-JS](https://github.com/getify/You-Dont-Know-JS)

  - github地址：https://github.com/getify/You-Dont-Know-JS.git

- [clean-code-javascript](https://github.com/ryanmcdermott/clean-code-javascript)

- **[ 30-seconds-of-code](https://github.com/30-seconds/30-seconds-of-code)**

## NodeJS

- **[ nodebestpractices（Node.js最佳实践）](https://github.com/goldbergyoni/nodebestpractices)**
  - github地址：https://github.com/goldbergyoni/nodebestpractices.git

