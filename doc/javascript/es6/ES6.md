# ES 新特性

## Object.assign(target, ...sources)

如果目标对象中的属性具有相同的键，则属性将被源对象中的属性覆盖。后面的源对象的属性将类似地覆盖前面的源对象的属性。



```javascript
const target = {a:1,b:2}
const source = {b:4, c:5}
const returnTarget = Object.assign(target, source)
console.log(target) //Object { a: 1, b: 4, c: 5 }
console.log(returnedTarget); // Object { a: 1, b: 4, c: 5 }
```