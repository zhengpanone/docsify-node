# Promise & 模块化

## promise **从⼊⻔到精通**

### **为什么需要** promise **？**

Javascript 是⼀⻔单线程语⾔，所以早期我们解决异步的场景时，⼤部分情况都是通过回调函数来进⾏。
例如在浏览器中发送 ajax 请求，就是常⻅的⼀个异步场景，发送请求后，⼀段时间服务端响应之后我们才能拿到结果。如果我们希望在异步结束之后执⾏某个操作，就只能通过回调函数这样的⽅式进⾏操作。

```javascript
var dynamicFunc = function(cb) {
setTimeout(function() {
 cb();
 }, 1000);
}
dynamicFunc(function() {console.log(123)});
```

例如上⾯这个例⼦，这⾥的 dynamicFunc 就是⼀个异步的函数，⾥⾯执⾏的 setTimeout 会在 1s 之后调⽤传⼊的 cb 函数。按照上⾯的调⽤⽅式，最终 1s 之后，会打印 123 这个结果。
同样的，如果后续还有内容需要在异步函数结束时输出的话，就需要多个异步函数进⾏嵌套，⾮常不利于后续的维护。

```javascript
setTimeout(function() {
 console.log(123);
 setTimeout(function() {
 console.log(321);
 // ...
 }, 2000);
}, 1000);
```

为了能使回调函数以更优雅的⽅式进⾏调⽤，在 `ES6` 中 `js` 产⽣了⼀个名为 promise 的新规范，他让异步操作的变得近乎「同步化」。

### Promise基础

在⽀持 `ES6` 的⾼级浏览器环境中，我们通过 new Promise() 即可构造⼀个 promise 实 例。
这个构造函数接受⼀个函数，分别接受两个参数，resolve 和 reject，代表着我们需要改变当前实例的状态到 已完成 或是 已拒绝 。



### 如何封装异步操作为promise

### promise规范解读

任何符合 promise 规范的对象或函数都可以成为 promise，promise A plus 规范地址：https://promisesaplus.com/上⾯我们熟悉了整体 promise 的⽤法，我们知道了如何去创建⼀个 promise，如何去使⽤它，后⾯我们也熟悉了如何去改造回调函数到 promise。本⼩节我们详细过⼀遍 promise A+ 规范，从规范层⾯明⽩promise 使⽤过程中的细节。

#### **术语**

Promise：promise 是⼀个拥有`then`⽅法的对象或函数，其⾏为符合本规范。

具有 then ⽅法（thenable）：是⼀个定义了 `then` ⽅法的对象或函数；

值（value）：指任何 JavaScript 的合法值（包括 undefined , thenable 和 promise）；

异常（exception）：是使⽤ throw 语句抛出的⼀个值。

原因（reason）：表示⼀个 promise 的拒绝原因。

#### **要求**

promise **的状态**

⼀个 Promise 的当前状态必须为以下三种状态中的⼀种：**等待态（Pending）**、**已完成（Fulfilled）**和**已拒绝（Rejected）**。

- 处于等待态时，promise 需满⾜以下条件：可以变为「已完成」或「已拒绝」

- 处于已完成时，promise 需满⾜以下条件：1. 不能迁移⾄其他任何状态 2. 必须拥有⼀个**不可变**的值

- 处于已拒绝时，promise 需满⾜以下条件：1. 不能迁移⾄其他任何状态 2. 必须拥有⼀个**不可变**的原因

**必须有⼀个** then **⽅法**

⼀个 promise 必须提供⼀个 then ⽅法以访问其当前值和原因。

promise 的 then ⽅法接受两个参数： `promise.then(onFulfilled, onRejected)` 他们都是可选参数，同时他

们都是函数，如果 `onFulfilled` 或 `onRejected` 不是函数，则需要忽略他们。

- 如果 `onFulfilled` 是⼀个函数
  - 当promise 执⾏结束后其必须被调⽤，其第⼀个参数为 promise 的结果在 
  - promise 执⾏结束前其不可被调⽤
  - 其调⽤次数不可超过⼀次

- 如果 `onRejected`是⼀个函数
  - 当 promise 被拒绝执⾏后其必须被调⽤，其第⼀个参数为 promise 的原因
  - 在 promise 被拒绝执⾏前其不可被调⽤
  - 其调⽤次数不可超过⼀次

在执⾏上下⽂堆栈仅包含平台代码之前，不得调⽤ `onFulfilled` 或 `onRejected`

`onFulfilled` 和 `onRejected`

必须被作为普通函数调⽤（即⾮实例化调⽤，这样函数内部 this ⾮严

格模式下指向 window）

then ⽅法可以被同⼀个 

promise 调⽤多次

当 

promise 

成功执⾏时，所有 onFulfilled 需按照其注册顺序依次回调

当 

promise 

被拒绝执⾏时，所有的 

onRejected 需按照其注册顺序依次回调

then ⽅法必须返回⼀个 promise 对象 

promise2 = promise1.then(onFulfilled, onRejected);

只要 

onFulfilled 

或者 

onRejected 

返回⼀个值 x 

，promise 2 都会进⼊ onFulfilled 状态

如果 

onFulfilled 

或者 

onRejected 

抛出⼀个异常 

e ，则 promise2 必须拒绝执⾏，并返回

拒因 

e

如果 

onFulfilled 

不是函数且 

promise1 

状态变为已完成， 

promise2 

必须成功执⾏并返回相

同的值

如果 onRejected 

不是函数且 

promise1 

状态变为已拒绝， 

promise2 

必须执⾏拒绝回调并返回相同的据因

### promise构造函数上的静态方法

### generator/async await简介





## JS **模块化从⼊⻔到精通**

