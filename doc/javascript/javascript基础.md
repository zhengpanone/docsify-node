# JavaScript基础

## 原始类型(primitive values)

除Object以外的所有类型都是不可变的(值本身无法被改变)

最新的ECMAScript标准定义了8种数据类型：

- 7种原始类型：
  - Boolean
  - Null
  - Undefined
  - Number
  - BigInt
  - String
  - Symbol
- 和Object