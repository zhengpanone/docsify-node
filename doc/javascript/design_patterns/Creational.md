# 创建型模式

## 单例模式

定义：保证一个类仅有一个实例，并且提供一个可以访问它的访问点

实现：用一个变量来标识实例是否已经存在，如果存在，则直接返回已经创建好的实例，反之就创建一个对象

场景：模态框、浏览器window对象，等等

简单实现 (ES6)：

```javascript
class Singleton{
    //构造函数
    constructor(name) {
        this.name = name
    }
    // 实例方法
    getName() {
        console.log(this.name);
    }
    //静态方法获取实例对象
    static getInstance(name) {
        if (!this.instance) {
            this.instance = new Singleton(name)
        }
        return this.instance
    }
}
// 创建静态属性作为唯一标识
Singleton.instance = null;
//验证
var a = Singleton.getInstance('a') 
var b = Singleton.getInstance('b') 
a.getName()//a
b.getName()//a
console.log(a===b); //true
```



## 工厂模式

定义：不暴露创建对象的具体逻辑，而是将将逻辑封装在一个函数中，这个函数被视为一个工厂；父类中提供一个创建对象的方法，允许子类决定实例化对象的类型

分类：简单工厂，工厂方法，抽象工厂

区别：简单工厂是将创建对象的步骤放在父类进行，工厂方法是延迟到子类中进行，它们两者都可以总结为：“根据传入的字符串来选择对应的类”，而抽象工厂则是：“用第一个字符串选择父类，再用第二个字符串选择子类”

### 简单工厂

### 工厂方法

### 抽象工厂



