<!-- doc/_coverpage.md -->

![logo](https://throwable-blog-1256189093.cos.ap-guangzhou.myqcloud.com/202009/_media/icon.svg)

# node book <small>0.1</small>

> 学习笔记。

- 简单、轻便 (压缩后 ~21kB)
- 无需生成 html 文件
- 众多主题

[GitHub](https://gitee.com/zhengpanone/docsify-node.git)
[Get Started](#docsify)